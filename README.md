## Google Bard Drupal Module

# Download
Please use the below project page link to download the latest module available for Drupal CMS.
Link: https://www.drupal.org/project/google_bard

# Dependency
- PHP CURL Library
- Google Bard account
- Google Bard should be working in the server 

# Installation
Please follow the below steps for module installation.
1. Download the google_bard module in "Docroot/module/contrib" directory
2. Extract the archive so that it would create a directory google_bard. In that case the final directory structure will be: Docroot/module/contrib/google_bard
3. Go to /admin/modules page as Drupal administrator
4. Find the Google Bard module under Generative AI group
5. Click on checkbox to select the module then hit the install button to complete the step.

# Google Bard API Keys
Please follow the below instructions to capture Google Bard API Keys require in the Drupal Bard module configuration step.
1. Open the link: https://bard.google.com in Google Chrome browser and click on "Sign in" button.
2. Post login, press Ctrl+Shift+I to open Developer tools in Google Chrome
3. Click on "Application" tab
4. On the left hand side under Storage listing, expand "Cookies" list. There will be an URL for https://bard.google.com. Click on it.
5. In the Filter textbox, write: 3psid. This will display only 2 cookies with the name __Secure-1PSID and __Secure-1PSIDCC
6. Note down the two values for next step

# Configuration
1. Go to /admin/modules page as Drupal administrator
2. Now in the module, a Configure option is available. Alternatively one can access it by using the URL: /admin/config/system/google-bard-settings
3. Assign the value of "__Secure-1PSID" to "API Key X" field and "__Secure-1PSIDCC" to "API Key Y" field then click on "Save configuration" button.

# Test Functionality
1. Go to /google_bard page
2. Type your query on "Message to Google Bard" textarea
3. Post that click on "Send" button
4. The module will post the question to Google Bard endpoint and fetch the response in the same page.