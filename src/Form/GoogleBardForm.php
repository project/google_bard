<?php

namespace Drupal\google_bard\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form class definition.
 */
class GoogleBardForm extends FormBase {

  /**
   * Define Form ID.
   */
  public function getFormId() {
    return 'google_bard_form';
  }

  /**
   * Build Form UI.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $bard_response_text = $this->getBardResponseText();

    // Add a markup element to display the Bard response text.
    $form['bard_response'] = [
      '#markup' => $bard_response_text,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to Google Bard'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * Define Form submit action.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Add your form submission logic here.
    $input_text = $form_state->getValue('message');

    try {
      // Call the getAnswer method within the try-catch block.
      $this->storeBardResponseText($input_text);
    }
    catch (\Exception $e) {
      // Handle the exception here.
      \Drupal::logger('google_bard')->error($e->getMessage());
      \Drupal::messenger()->addError($this->t('An error occurred while retrieving Bard response text. Please try again later.'), 'error');
    }
  }

  /**
   * Get the Bard response text.
   */
  private function getBardResponseText() {
    $html_answer_text = 'Due to some technical issue, Google Bard is unable to
    deliver the answer of your query. Please change or rephrase your question
    and try again later.';
    // Retrieve the Bard response text from where it is stored.
    $response_text = \Drupal::state()->get('bard_response_text');
    if (!empty($response_text['textQuery']) && !empty($response_text['choices'])) {
      $answer_text = '<strong>Question: ' . $response_text['textQuery'][0] . '</strong><br>' . $response_text['choices'][0]['content'][0];
      $html_answer_text = $this->markdownToHtml($answer_text);
    }

    return $html_answer_text;
  }

  /**
   * Store the Bard response text.
   */
  private function storeBardResponseText($text) {
    // Two keys are required which are two cookies values.
    $config = \Drupal::config('google_bard.settings');

    $_ENV['BARD_API_KEY_X'] = $config->get('api_key_x');
    $_ENV['BARD_API_KEY_Y'] = $config->get('api_key_y');
    $bard = new Bard();
    $result = $bard->getAnswer($text);
    \Drupal::state()->set('bard_response_text', $result);
  }

  /**
   * Convert markdown text to HTML format.
   */
  private function markdownToHtml($markdown) {

    // Convert Markdown links.
    $markdown = preg_replace('/\[([^\]]+)\]\(([^\)]+)\)/', '<a href="$2">$1</a>', $markdown);
    // Convert Markdown bold.
    $markdown = preg_replace('/\*\*([^\*]+)\*\*/', '<strong>$1</strong>', $markdown);
    // Convert Markdown italic.
    $markdown = preg_replace('/_([^_]+)_/', '<em>$1</em>', $markdown);
    // Convert Markdown headers (## Header 2).
    $markdown = preg_replace('/## ([^\n]+)/', '<h2>$1</h2>', $markdown);
    // Convert Markdown headers (# Header 1).
    $markdown = preg_replace('/# ([^\n]+)/', '<h1>$1</h1>', $markdown);
    // Convert Markdown paragraphs.
    $markdown = preg_replace('/\n\n/', '</p><p>', $markdown);
    // Convert unordered lists (bullet points).
    $markdown = preg_replace('/(\n\s*)?(?:\*|\-)\s+([^\n]+)/', '$1<ul><li>$2</li></ul>', $markdown);
    // Wrap paragraphs in <p> tags.
    $markdown = "<p>{$markdown}</p>";
    // Convert PHP code blocks.
    $markdown = preg_replace('/```php\s+([^`]+)\s+```/', '<code>$1</code>', $markdown);
    // Convert Java code blocks.
    $markdown = preg_replace('/```java\s+([^`]+)\s+```/s', '<code>$1</code>', $markdown);

    return $markdown;
  }

}
