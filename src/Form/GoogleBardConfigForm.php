<?php

namespace Drupal\google_bard\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config form class definition.
 */
class GoogleBardConfigForm extends ConfigFormBase {

  /**
   * Define name.
   */
  protected function getEditableConfigNames() {
    return ['google_bard.settings'];
  }

  /**
   * Define Form ID.
   */
  public function getFormId() {
    return 'google_bard_config_form';
  }

  /**
   * Build Form UI.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_bard.settings');

    $form['api_key_x'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key X'),
      '#default_value' => $config->get('api_key_x'),
      '#description' => $this->t('Value of cookie "__Secure-1PSID" captured from https://bard.google.com'),
    ];

    $form['api_key_y'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key Y'),
      '#default_value' => $config->get('api_key_y'),
      '#description' => $this->t('Value of cookie "__Secure-1PSIDCC" captured from https://bard.google.com'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Define Form submit action.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('google_bard.settings');
    $config->set('api_key_x', $form_state->getValue('api_key_x'));
    $config->set('api_key_y', $form_state->getValue('api_key_y'));
    $config->save();

    parent::submitForm($form, $form_state);

  }

}
