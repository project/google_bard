<?php

namespace Drupal\google_bard\Form;

/**
 * Bard class definition.
 */
class Bard {

  /**
   * Proxy Configuration.
   *
   * @var string
   */
  private $proxies;

  /**
   * Timeout setting.
   *
   * @var int
   */
  private $timeout;

  /**
   * Session ID.
   *
   * @var string
   */
  private $session;

  /**
   * Populated by Google Bard.
   *
   * @var string
   */
  private $conversationID;

  /**
   * Populated by Google Bard.
   *
   * @var string
   */
  private $responseID;

  /**
   * Populated by Google Bard.
   *
   * @var int
   */
  private $choiceID;

  /**
   * Private variable $reqid.
   *
   * @var int
   */
  private $reqid;

  /**
   * Private variable $snlm0e.
   *
   * @var string
   */
  private $snlm0e;

  /**
   * Private variable $cfb2h.
   *
   * @var string
   */
  private $cfb2h;

  /**
   * Constructor definition.
   */
  public function __construct($timeout = 300, $proxies = NULL, $session = NULL) {
    $this->proxies = $proxies;
    $this->timeout = $timeout;
    $headers = [
      "Host: bard.google.com",
      "X-Same-Domain: 1",
      "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36",
      "Content-Type: application/x-www-form-urlencoded;charset=UTF-8",
      "Origin: https://bard.google.com",
      "Referer: https://bard.google.com/",
    ];
    $this->reqid = (int) rand(pow(10, 3 - 1), pow(10, 3) - 1);
    $this->conversationID = "";
    $this->responseID = "";
    $this->choiceID = "";

    if ($session === NULL) {
      $this->session = curl_init();
      curl_setopt($this->session, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($this->session, CURLOPT_COOKIE, "__Secure-1PSID=" . $_ENV["BARD_API_KEY_X"] . "; __Secure-1PSIDTS=" . $_ENV["BARD_API_KEY_Y"]);
      curl_setopt($this->session, CURLOPT_RETURNTRANSFER, TRUE);
    } else {
      $this->session = $session;
    }

    $this->snlm0e = $this->getSnim0e();
  }

  /**
   * Perform CURL Request on Bard endpoint.
   */
  private function getSnim0e() {
    curl_setopt($this->session, CURLOPT_URL, "https://bard.google.com/");
    curl_setopt($this->session, CURLOPT_TIMEOUT, $this->timeout);
    curl_setopt($this->session, CURLOPT_PROXY, $this->proxies);
    curl_setopt($this->session, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    curl_setopt($this->session, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($this->session, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($this->session, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($this->session, CURLOPT_RETURNTRANSFER, TRUE);
    $resp = curl_exec($this->session);
    if (curl_getinfo($this->session, CURLINFO_HTTP_CODE) !== 200) {
      throw new \Exception("Response Status: " . curl_getinfo($this->session, CURLINFO_HTTP_CODE));
    }
    preg_match('/"SNlM0e":"(.*?)"/', $resp, $matches);
    preg_match('/"cfb2h":"(.*?)"/', $resp, $matchesCfb2h);
    $this->cfb2h = $matchesCfb2h[1];
    return $matches[1];
  }

  /**
   * Fetch the answer from Google Bard.
   */
  public function getAnswer($input_text) {
    $params = [
      "bl" => $this->cfb2h,
      "_reqid" => (string) $this->reqid,
      "rt" => "c",
    ];
    $input_text_struct = [
      [$input_text],
      NULL,
      [$this->conversationID, $this->responseID, $this->choiceID],
    ];
    $data = [
      "f.req" => json_encode([NULL, json_encode($input_text_struct)]),
      "at" => $this->snlm0e,
    ];
    curl_setopt($this->session, CURLOPT_URL, "https://bard.google.com/_/BardChatUi/data/assistant.lamda.BardFrontendService/StreamGenerate?" . http_build_query($params));
    curl_setopt($this->session, CURLOPT_POST, TRUE);
    curl_setopt($this->session, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($this->session, CURLOPT_TIMEOUT, $this->timeout);
    curl_setopt($this->session, CURLOPT_PROXY, $this->proxies);
    curl_setopt($this->session, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
    curl_setopt($this->session, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($this->session, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($this->session, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($this->session, CURLOPT_RETURNTRANSFER, TRUE);
    $resp = curl_exec($this->session);
    $resp_dict = json_decode(explode("\n", $resp)[3], TRUE)[0][2];
    if ($resp_dict === NULL) {
      return ["content" => "Response Error: " . $resp . "."];
    }
    $parsed_answer = json_decode($resp_dict, TRUE);
    $bard_answer = [
      "content" => $parsed_answer[0],
      "conversationID" => $parsed_answer[1][0],
      "responseID" => $parsed_answer[1][1],
      "factualityQueri-es" => $parsed_answer[3],
      "textQuery" => $parsed_answer[2][0] ?? "",
      "choices" => is_array($parsed_answer[4]) ? array_map(function ($i) {
        return ["id" => $i[0], "content" => $i[1]];
      }, $parsed_answer[4]) : [],
    ];
    $this->conversationID = $bard_answer["conversationID"];
    $this->responseID = $bard_answer["responseID"];
    // Make sure "choices" is not empty before accessing index 0.
    if (!empty($bard_answer["choices"])) {
      $this->choiceID = $bard_answer["choices"][0]["id"];
      $this->reqid += 100000;
      return $bard_answer;
    }

  }

}
